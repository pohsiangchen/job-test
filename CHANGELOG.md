# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.12](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.11...v1.2.12) (2020-12-03)

### [1.2.11](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.10...v1.2.11) (2020-12-03)

### [1.2.10](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.9...v1.2.10) (2020-12-03)

### [1.2.9](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.8...v1.2.9) (2020-12-03)

### [1.2.8](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.7...v1.2.8) (2020-12-02)

### [1.2.7](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.6...v1.2.7) (2020-12-02)

### [1.2.6](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.5...v1.2.6) (2020-12-02)


### Bug Fixes

* test ([5c5ed35](https://gitlab.com/pohsiangchen/job-test/commit/5c5ed35c14c9e802b4b2fdba3cc43373c03ada57))

### [1.2.5](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.4...v1.2.5) (2020-12-02)

### [1.2.4](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.3...v1.2.4) (2020-12-02)

### [1.2.3](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.2...v1.2.3) (2020-12-02)


### Bug Fixes

* test ([b8d6993](https://gitlab.com/pohsiangchen/job-test/commit/b8d699357104e7334dcd0d33df3de9b1f3ce94a2))

### [1.2.2](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.1...v1.2.2) (2020-12-02)

### [1.2.1](https://gitlab.com/pohsiangchen/job-test/compare/v1.2.0...v1.2.1) (2020-12-02)

## [1.2.0](https://gitlab.com/pohsiangchen/job-test/compare/v1.1.1...v1.2.0) (2020-12-02)


### Features

* **index.js:** feature hello ([fed747f](https://gitlab.com/pohsiangchen/job-test/commit/fed747f39cb559037b04e175f0a1b0c8f2101530))
* **test.js:** feature moon ([f1c3f19](https://gitlab.com/pohsiangchen/job-test/commit/f1c3f199e9d183be95d5ba3c9e3c3629cf7279fa))


### Bug Fixes

* **test.js:** fix test file ([3e46c28](https://gitlab.com/pohsiangchen/job-test/commit/3e46c28eef33279d0cf5d435efeb4719cf3df1e8))

### [1.1.1](https://gitlab.com/pohsiangchen/job-test/compare/v1.1.0...v1.1.1) (2020-12-02)


### Bug Fixes

* **index:** test ([8327c61](https://gitlab.com/pohsiangchen/job-test/commit/8327c61f49d4e564aea4350b708f50cafe939b24))
* test ([bef1d73](https://gitlab.com/pohsiangchen/job-test/commit/bef1d73fdf4df27a74e83d61d6b4cc581e0acc39))

## [1.1.0](https://gitlab.com/pohsiangchen/job-test/compare/v1.0.3...v1.1.0) (2020-12-02)


### Features

* test2 ([779e015](https://gitlab.com/pohsiangchen/job-test/commit/779e01522d8be88d78eb75a01d25f1b2d3b95a0c))

### [1.0.3](https://gitlab.com/pohsiangchen/job-test/compare/v1.0.2...v1.0.3) (2020-12-02)


### Docs

* version ([664e452](https://gitlab.com/pohsiangchen/job-test/commit/664e45217f888934f6878a558ae8a9bfce21106c))

### [1.0.2](https://gitlab.com/pohsiangchen/job-test/compare/v1.0.1...v1.0.2) (2020-12-02)

### 1.0.1 (2020-12-02)


### Bug Fixes

* test ([9d8760d](https://gitlab.com/pohsiangchen/job-test/commit/9d8760d3203f4cfad439cc0d6d12384a224a4a32))
